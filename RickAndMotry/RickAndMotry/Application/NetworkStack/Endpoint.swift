//
//  Endpoints.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import Foundation

enum Endpoint: String {
    case imagedatamodel = "https://rickandmortyapi.com/api/character/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,100,101,102"
}
