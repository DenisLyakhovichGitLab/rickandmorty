//
//  NetworkManager.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import Foundation
import UIKit

class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager()
    private override init() {}
    var dataProvider: [AnyObject] = []
    var imageCache = NSCache<NSString, UIImage>()
    
    //MARK: - RequestData API
    func loadDataApi<T: Decodable>(url: Endpoint, model: T.Type, completion: @escaping (Any) -> Void) {
        guard let urlData = URL(string: url.rawValue) else { return }
        let task = URLSession.shared.dataTask(with: urlData) { [self] data, _, _ in
            guard let data = data else { return }
            guard let jsonData = try? JSONDecoder().decode(model.self, from: data) else {
                return
            }
            self.dataProvider = jsonData as! [AnyObject]
            print(self.dataProvider)
            completion(self.dataProvider)
        }
        task.resume()
    }
    
    //MARK: - RequestImage API
    func loadDataApiImage(url: String, imageUI: UIImageView ) -> UIImage {
        if let cachedImage = imageCache.object(forKey: url as NSString) {
            return cachedImage
        } else {
            let urlData = URL(string: url)
            URLSession.shared.dataTask(with: urlData!, completionHandler: { (data, _, _) in
                DispatchQueue.main.async {
                    imageUI.image = UIImage(data: data!)
                    self.imageCache.setObject(imageUI.image!, forKey: url as NSString)
                }
                    
            }).resume()
            return UIImage()
        }
    }
}
