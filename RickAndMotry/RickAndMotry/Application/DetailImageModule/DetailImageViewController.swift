//
//  DetailImageViewController.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import UIKit

class DetailImageViewController: UIViewController {

    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    var nameDetail = ""
    var createdDeatail = ""
    var urlImageDetail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        loadImage()
    }
    
    func loadData() {
        nameLable.text = nameDetail
        createdLabel.text = createdDeatail
    }
    
    func loadImage() {
        viewImage.image = NetworkManager.sharedInstance.loadDataApiImage(url: urlImageDetail, imageUI: viewImage)
    }
}
