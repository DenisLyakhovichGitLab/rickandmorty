//
//  ImageDataModel.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import Foundation

struct ImageDataModel: Decodable {
    let id: Int64
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
    let origin: OriginData
    let location: LocationData
    let image: String
    let episode: [String]
    let url: String
    let created: String
}

struct OriginData: Decodable {
    let name: String
    let url: String
}

struct LocationData: Decodable {
    let name: String
    let url: String
}
