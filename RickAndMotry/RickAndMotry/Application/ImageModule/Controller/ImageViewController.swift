//
//  ViewController.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import UIKit

class ImageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    var reuseIdentifire = "imagecell"
    var data: [ImageDataModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifire)
        loadData()
        refreshData()
    }
    
    // MARK: tableview Delegate, DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as? ImageTableViewCell
        cell?.dataCell = data[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    // MARK: SetNavigation and pass data in LadderDetailViewController
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        let goToVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailImageViewController") as? DetailImageViewController
        goToVc?.nameDetail = item.name
        goToVc?.createdDeatail = item.created
        goToVc?.urlImageDetail = item.image
        navigationController?.pushViewController(goToVc!, animated: true)
    }
    
    // MARK: LoadData from API
    private func loadData() {
        NetworkManager.sharedInstance.loadDataApi(url: .imagedatamodel, model: [ImageDataModel].self) { dataProvider in
            self.data = dataProvider as! [ImageDataModel]
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: SetRefreshData
    func refreshData() {
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(nil, action: #selector (refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh() {
        loadData()
        alertData()
        refreshControl.endRefreshing()
    }
    
    // MARK: SetAlert, if no data API
    func alertData() {
        if data.isEmpty {
            let alert = UIAlertController(title: "Отсутствуют данные",
                                          message: "Сервер не отвечает или нет сети",
                                          preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",
                                         style: .default) 
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
}
