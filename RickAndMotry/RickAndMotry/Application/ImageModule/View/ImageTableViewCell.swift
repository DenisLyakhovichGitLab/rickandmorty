//
//  ImageTableViewCell.swift
//  RickAndMotry
//
//  Created by Denis Lyakhovich on 11.03.2022.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var dataCell: ImageDataModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewImage.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if let data = dataCell {
            nameLabel.text = data.name
            viewImage.image = NetworkManager.sharedInstance.loadDataApiImage(url: data.image, imageUI: viewImage)
        }
    }
    
}
